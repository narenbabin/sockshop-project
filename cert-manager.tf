resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = "cert-manager"
  force_update  = true
  set {
    name = "installCRDs"
    value = "true"
  }
  depends_on = [module.gke, resource.kubernetes_manifest.namespace_cert_manager]
}