provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "k8s-project"
  region      = "us-central1"
}

provider "google-beta" {
  credentials = file("./creds/serviceaccount.json")
  project     = "k8s-project"
  region      = "us-central1"
}

provider "kubectl" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  load_config_file       = false
}

provider "kubernetes" {
  experiments {
    manifest_resource = true
  }
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  config_path    = "~/.kube/config"
  config_context = "gke_trim-bonsai-326906_us-central1_k8s-cluster"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

terraform {
  required_version = ">= 0.13"

  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}