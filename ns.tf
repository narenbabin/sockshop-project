resource "kubernetes_manifest" "namespace_traefik" {
  manifest = {
    "apiVersion" = "v1"
    "kind" = "Namespace"
    "metadata" = {
      "name" = "traefik"
    }
  }
}

resource "kubernetes_manifest" "namespace_flux" {
  manifest = {
    "apiVersion" = "v1"
    "kind" = "Namespace"
    "metadata" = {
      "name" = "flux"
    }
  }
}

resource "kubernetes_manifest" "namespace_monitoring" {
  manifest = {
    "apiVersion" = "v1"
    "kind" = "Namespace"
    "metadata" = {
      "name" = "monitoring"
    }
  }
}

resource "kubernetes_manifest" "namespace_kube_logging" {
  manifest = {
    "apiVersion" = "v1"
    "kind" = "Namespace"
    "metadata" = {
      "name" = "kube-logging"
    }
  }
}

resource "kubernetes_manifest" "namespace_cert_manager" {
  manifest = {
    "apiVersion" = "v1"
    "kind" = "Namespace"
    "metadata" = {
      "name" = "cert-manager"
    }
  }
}
