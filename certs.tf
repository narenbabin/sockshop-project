resource "kubectl_manifest" "certs" {
    yaml_body = <<YAML
---
apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    email: timoshchenkoaa@gmail.com
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      # Secret resource used to store the account's private key.
      name: your-own-very-secretive-key
    solvers:
      - http01:
          ingress:
            class: traefik-cert-manager
---
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: sock-shop-cert
  namespace: sockshop
spec:
  commonName: sock-shop.34.132.176.130.nip.io
  secretName: sock-shop-cert
  dnsNames:
    - sock-shop.34.132.176.130.nip.io
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
---
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: grafana-cert
  namespace: monitoring
spec:
  commonName: grafana.34.132.176.130.nip.io
  secretName: grafana-cert
  dnsNames:
    - grafana.34.132.176.130.nip.io
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
---
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: prom-cert
  namespace: monitoring
spec:
  commonName: prometheus.34.132.176.130.nip.io
  secretName: prom-cert
  dnsNames:
    - prometheus.34.132.176.130.nip.io
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
---
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: kibana-cert
  namespace: kube-logging
spec:
  commonName: kibana.34.132.176.130.nip.io
  secretName: kibana-cert
  dnsNames:
    - kibana.34.132.176.130.nip.io
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
---
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: jaeger-cert
  namespace: sockshop
spec:
  commonName: jaeger.34.132.176.130.nip.io
  secretName: jaeger-cert
  dnsNames:
    - jaeger.34.132.176.130.nip.io
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer            
    
YAML
  depends_on = [module.gke, 
    kubernetes_manifest.namespace_monitoring, 
    kubernetes_manifest.namespace_kube_logging,
    kubernetes_manifest.namespace_sockshop,
    kubernetes_manifest.namespace_traefik]
}